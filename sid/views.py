from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect
from django.db import models, transaction
from django.urls import reverse
from django.contrib import messages

#Modelos
from .models import Vector
from .models import VectorDistribucion
from .models import VectorModelo
from .models import Configuracion

# Librerias
import pandas as pd
import numpy as np
import random

#Paginas de error
def handler404(request, *args, **argv):
    return render(request, '404.html')

def handler500(request, *args, **argv):
    return render(request, '500.html')

#Pagina inicial
def index(request):
	#Se crea la configuracion inicial si no existe, o se actualiza si existe
	validarConfiguracionInicial(obtenerCantidadVectores())
	return render(request, 'index.html')

#Pagina de generacion de datos
def generar(request):
	#Se crea la configuracion inicial si no existe, o se actualiza si existe
	validarConfiguracionInicial(obtenerCantidadVectores())
	totalVectores = obtenerTotalVectores()
	vectorActual = obtenerVectorActual()
	vectoresGenerados = obtenerVectoresGenerados()
	
	return render(request, 'generar.html', 
		{
			'totalVectores': totalVectores, 
			'vectorActual': vectorActual, 
			'vectoresGenerados': vectoresGenerados
		}
	)

#Pagina de visualizacion de datos
def visualizar(request):
	#Se crea la configuracion inicial si no existe, o se actualiza si existe
	validarConfiguracionInicial(obtenerCantidadVectores())
	totalVectores = obtenerTotalVectores()
	vistaTabla = ''
	columnasDataframe = {}
	estadisticas = []
	ecuaciones = []
	correlaciones = []
	ejesYError = []
	rsme = []
	cantidadDatos = 300

	try:
		#Se usa para generar la excepcion en el caso de que la tabla de vectores esta vacia
		Vector.objects.get(id = 1)

		df = pd.DataFrame(list(Vector.objects.all().values()))
		arr = np.array(df)

		#Se generan los datos del eje x
		ejeX = arr[:, 0].tolist()
		ejeX = list(map(lambda x: "{:.2f}".format(x), ejeX))
		ejeX = np.array(ejeX).astype('float64')
		ejesY = []
		ejesYModelo = []
		mostrarGrafico = 1
		columnasDataframe["X"] = ejeX

		#Se generan los datos de los vectores
		for i in range(1, totalVectores + 1):
			arrEjeY = arr[:, i]
			ejeY = arrEjeY.tolist()
			ejeY = list(map(lambda x: "{:.2f}".format(x), ejeY))
			ejeY = np.array(ejeY).astype('float64')
			ejesY.append(ejeY[:cantidadDatos].tolist()) #Se muestran solo los primeros n registros
			ejesYModelo.append(ejeY) #Se usa para generar el modelo con todos los datos

			estadisticas.append({
				'maximo': arrEjeY.max(),
				'minimo': arrEjeY.min(),
				'promedio': round(arrEjeY.mean(), 2),
				'desviacion': round(arrEjeY.std(), 2)
			})

			columnasDataframe["Y"+str(i)] = ejeY

		ecuaciones = generarModeloLinealGradoN(ejeX, ejesYModelo)
		correlaciones = generarCorrelaciones(ejesYModelo)
		columnasDataframe, ejesYError, rsme = agregarEcuacionesDataFrame(ecuaciones, columnasDataframe, ejesYModelo, cantidadDatos)

		#Se crea el html con los datos de la tabla
		dataFrame = pd.DataFrame(columnasDataframe)
		vistaTabla = dataFrame.to_html(index = False, justify = 'center', classes = 'table table-striped table-sm table-borderless text-center')
	except Vector.DoesNotExist:
		ejeX = []
		ejesY = []
		mostrarGrafico = 0

	return render(request, 'visualizar.html', 
		{
			'ejeX': ejeX[:cantidadDatos].tolist(), #Se muestran solo los primeros n registros
			'ejesY': ejesY, 
			'mostrarGrafico': mostrarGrafico, 
			'vistaTabla': vistaTabla,
			'estadisticas': estadisticas, 
			'ecuaciones': ecuaciones,
			'correlaciones': correlaciones,
			'ejesYError': ejesYError,
			'rsme': rsme
		}
	)

#Funcion para configurar la cantidad de vectores a usar en la aplicacion
def obtenerCantidadVectores():
	cantidadVectores = 6
	return cantidadVectores

#Funcion para ingresar o actualizar los datos en la BD
def gestionarDatos(request):
	totalVectores = obtenerTotalVectores()
	vectorActual = obtenerVectorActual()
	lista = []
	maximo = 300
	
	try:
		distribucion = request.POST["distribucion"]
	except Exception as e:
		distribucion = "discreta"

	#Si no hay vectores generados, lo primero es insertar
	if vectorActual == 0:
		actualizar = 0
	else:
		actualizar = 1

	#Definicion de columna a actualizar
	if totalVectores == 1:
		siguienteVector = 1
		columna = "arreglo1"
	else:
		siguienteVector = vectorActual + 1

		if siguienteVector > totalVectores:
			siguienteVector = 1

		columna = "arreglo%d" % siguienteVector

	#Generacion de numeros aleatorios
	for i in range(0, 1000):
		#Crea el numero dependiendo del tipo de distribucion
		if distribucion == "discreta":
			numero = random.randint(0, maximo)
		else:
			numero = round(random.random() * maximo, 2)

		if actualizar == 0:
			#Datos para insertar
			v = Vector(id = i+1, arreglo1 = numero)
		else:
			#Datos para actualizar
			v = Vector.objects.get(id = i+1)
			
			if siguienteVector == 1:
				v.arreglo1 = numero
			elif siguienteVector == 2:
				v.arreglo2 = numero
			elif siguienteVector == 3:
				v.arreglo3 = numero
			elif siguienteVector == 4:
				v.arreglo4 = numero
			elif siguienteVector == 5:
				v.arreglo5 = numero
			else:
				v.arreglo6 = numero

		lista.append(v)
	
	#Se crean/actualizan todas las filas en bloque
	configuracion = Configuracion.objects.get(id = 1)
	
	if actualizar == 0:
		#bulk_create inserta los datos en bloque
		Vector.objects.bulk_create(lista)
		configuracion.vectorActual = 1
	else:
		#La transaccion hace que solo se produzca un update masivo
		with transaction.atomic():
			for v in lista:
				v.save()
		
		#Se actualiza el campo de vector actual en la tabla de configuracion
		configuracion.vectorActual = siguienteVector
	
	configuracion.save()
	
	messages.success(request, "Vector " + str(siguienteVector) + " generado exitosamente!")
	return redirect('generar')

#Funcion para crear la configuracion inicial si no existe, o para actualizarla si existe
def validarConfiguracionInicial(cantidadVectores):
	try:
	    configuracion = Configuracion.objects.get(id = 1)
	    configuracion.totalVectores = cantidadVectores
	except Configuracion.DoesNotExist:
	    configuracion = Configuracion(id = 1, totalVectores = cantidadVectores, vectorActual = 0)
	
	configuracion.save()

#Funcion que obtiene el campo totalVectores de la tabla de configuracion
def obtenerTotalVectores():
	try:
	    totalVectores = Configuracion.objects.get(id = 1).totalVectores		    
	except Configuracion.DoesNotExist:
	    totalVectores = 1

	return totalVectores

#Funcion que obtiene el campo vectorActual de la tabla de configuracion
def obtenerVectorActual():
	try:
		configuracion = Configuracion.objects.get(id = 1)
		vectorActual = configuracion.vectorActual
		totalVectores = configuracion.totalVectores

		if vectorActual > totalVectores:
			vectorActual = totalVectores
	except Configuracion.DoesNotExist:
	    vectorActual = 0

	return vectorActual

#Funcion que obtiene los vectores generados
def obtenerVectoresGenerados():
	try:
		vectoresGenerados = 0
		vectoresTotales = obtenerCantidadVectores()
		vector = Vector.objects.get(id = 1)

		if vector.arreglo1 >= 0:
			vectoresGenerados += 1

		if vector.arreglo2 >= 0:
			vectoresGenerados += 1

		if vector.arreglo3 >= 0:
			vectoresGenerados += 1

		if vector.arreglo4 >= 0:
			vectoresGenerados += 1

		if vector.arreglo5 >= 0:
			vectoresGenerados += 1

		if vector.arreglo6 >= 0:
			vectoresGenerados += 1

		if vectoresGenerados > vectoresTotales:
			vectoresGenerados = vectoresTotales
	except Vector.DoesNotExist:
	    vectoresGenerados = 0

	return vectoresGenerados

#Funcion para generar el modelo matematico de una forma lineal de grado N
def generarModeloLinealGradoN(ejeX, ejesY):
	ecuaciones = []
	indice = 0
	grado = 40

	for ejeY in ejesY:
		#Se generan los coeficientes y la funcion
		coefs, res, _, _, _ = np.polyfit(ejeX, ejeY, grado, full = True)
		ejeYGrafico = list(np.polyval(coefs, ejeX))
		funcion = "y = "
		indiceGrado = grado

		for c in coefs:
			if c != 0:
				n = str(abs(c))
				prefijo = ""

				if c > 0:
					if funcion != "y = ":
						prefijo = " + "
				else:
					prefijo = " - "

				if indiceGrado == grado:
					funcion += n + " x<sup>" + str(indiceGrado) + "</sup>"
				elif indiceGrado > 0 and indiceGrado != 1:
					funcion += prefijo + n + " x<sup>" + str(indiceGrado) + "</sup>"
				elif indiceGrado == 1:
					funcion += prefijo + n + " x"
				else:
					funcion += prefijo + n

			indiceGrado -= 1

		columnasDataframe = {
			'x': ejeX,
			'y': ejeY
		}

		#Se genera el dataframe y se crean las columnas x2 y xy
		df = pd.DataFrame(columnasDataframe)
		df['x2'] = np.power(df['x'], 2)
		df['xy'] = np.multiply(df['x'], df['y'])

		#Se obtienen los totales de las columnas
		sumaX = df['x'].sum()
		sumaY = df['y'].sum()
		sumaX2 = df['x2'].sum()
		sumaXY = df['xy'].sum()

		#Se halla el coeficiente de correlacion lineal r
		n = df.shape[0]
		mediaX = sumaX / n
		mediaY = sumaY / n

		df['x-Px'] = np.subtract(df['x'], mediaX)
		df['y-Py'] = np.subtract(df['y'], mediaY)
		df['x-Px2'] = np.power(df['x-Px'], 2)
		df['y-Py2'] = np.power(df['y-Py'], 2)
		df['x-Pxy-Py'] = np.multiply(df['x-Px'], df['y-Py'])

		sumaXPX = df['x-Px2'].sum()
		sumaYPY = df['y-Py2'].sum()
		sumaXPXYPX = df['x-Pxy-Py'].sum()

		# r = sumaXPXYPX / (sqrt(sumaXPX) * sqrt(sumaYPY))
		r = sumaXPXYPX / (np.sqrt(sumaXPX) * np.sqrt(sumaYPY))

		ecuaciones.append({
			'i': indice,
			'r': round(r, 10),
			'ejeY': ejeYGrafico,
			'ecuacion': funcion
		})

		indice += 1

	return ecuaciones

#Funcion para generar el modelo matematico de una forma lineal de grado 1
def generarModeloLineal(ejeX, ejesY):
	ecuaciones = []
	indice = 0

	for ejeY in ejesY:
		ejeYGrafico = []
		columnasDataframe = {
			'x': ejeX,
			'y': ejeY
		}

		#Se genera el dataframe y se crean las columnas x2 y xy
		df = pd.DataFrame(columnasDataframe)
		df['x2'] = np.power(df['x'], 2)
		df['xy'] = np.multiply(df['x'], df['y'])

		#Se obtienen los totales de las columnas
		sumaX = df['x'].sum()
		sumaY = df['y'].sum()
		sumaX2 = df['x2'].sum()
		sumaXY = df['xy'].sum()

		# Formulas para ecuaciones de la forma: y = mx + b
		# n: Tamaño de la muestra
		# m = ((n * sumaXY) - (sumaX * sumaY)) / ((n * sumaX2) - (sumaX * sumaX))
		# b = (sumaY - (m * sumaX)) / n
		n = df.shape[0]
		m = ((n * sumaXY) - (sumaX * sumaY)) / ((n * sumaX2) - np.power(sumaX, 2))
		b = (sumaY - (m * sumaX)) / n

		#Se aplica la ecuacion para hallar los puntos de la linea recta
		for x in ejeX:
			y = (m * x) + b
			ejeYGrafico.append(y)

		#Se halla el coeficiente de correlacion lineal r
		mediaX = sumaX / n
		mediaY = sumaY / n

		df['x-Px'] = np.subtract(df['x'], mediaX)
		df['y-Py'] = np.subtract(df['y'], mediaY)
		df['x-Px2'] = np.power(df['x-Px'], 2)
		df['y-Py2'] = np.power(df['y-Py'], 2)
		df['x-Pxy-Py'] = np.multiply(df['x-Px'], df['y-Py'])

		sumaXPX = df['x-Px2'].sum()
		sumaYPY = df['y-Py2'].sum()
		sumaXPXYPX = df['x-Pxy-Py'].sum()

		# r = sumaXPXYPX / (sqrt(sumaXPX) * sqrt(sumaYPY))
		r = sumaXPXYPX / (np.sqrt(sumaXPX) * np.sqrt(sumaYPY))

		if b >= 0:
		    ecuaciones.append({
		    	'i': indice,
		    	'r': round(r, 10),
		    	'ejeY': ejeYGrafico,
		    	'ecuacion': 'y = ' + str(round(m, 5)) + 'x + ' + str(round(b, 5))
	    	})
		else:
		    ecuaciones.append({
		    	'i': indice,
		    	'r': round(r, 10),
		    	'ejeY': ejeYGrafico,
		    	'ecuacion': 'y = ' + str(round(m, 5)) + 'x - ' + str(round(np.abs(b), 5))
	    	})

		indice += 1

	return ecuaciones

#Funcion que genera las correlaciones por cada pareja de vectores
def generarCorrelaciones(ejesY):
	correlaciones = []

	for i in range(0, len(ejesY)):
		correlaciones.append([])

		for j in range(0, len(ejesY)):
			corr = np.corrcoef(ejesY[i], ejesY[j])
			r = round(corr[0][1], 5)
			correlaciones[i].append(r)

	return correlaciones

#Funcion que agrega las columnas de los datos evaluados en la funcion y el error de vlidacion con los originales
def agregarEcuacionesDataFrame(ecuaciones, columnasDataframe, ejesYModelo, cantidadDatos):
	decimales = 2
	ejesYError = []
	rsme = []

	for ecu in ecuaciones:
		columnasDataframe["Y-Ec"+str(ecu['i'] + 1)] = np.round(ecu['ejeY'], decimales)
	
	for ecu in ecuaciones:
		ejeYError = np.abs(np.subtract(np.round(ejesYModelo[ecu['i']], decimales), np.round(ecu['ejeY'], decimales)))
		ejesYError.append(ejeYError[:cantidadDatos].tolist())
		columnasDataframe["Error Y"+str(ecu['i'] + 1)] = ejeYError

		errorCuadrado = np.power(ejeYError, 2)
		valor = np.round(np.sqrt(errorCuadrado.sum() / len(ejeYError)), decimales)
		rsme.append(valor)

	return columnasDataframe, ejesYError, rsme

#Funcion para cargar un archivo csv con datos para reemplazar el vector seleccionado
def uploadCsv(request):
	validarConfiguracionInicial(obtenerCantidadVectores())
	totalVectores = obtenerTotalVectores()

	#Informacion de la pagina
	if request.method == "GET":
		return render(request, 'cargar.html', {'totalVectores': range(totalVectores)})

	#Informacion del formulario
	try:
		archivoCsv = request.FILES["archivoCsv"]
		vector = request.POST["vector"]

		if not archivoCsv.name.endswith('.csv'):
			messages.error(request, 'El archivo no es de tipo CSV')
			return HttpResponseRedirect(reverse("cargarCSV"))

        #Se verifica el peso del archivo (2 MB)
		if archivoCsv.multiple_chunks():
			messages.error(request, "El archivo es muy grande (%.2f MB)." % (archivoCsv.size / (1000*1000)))
			return HttpResponseRedirect(reverse("cargarCSV"))

		archivo = archivoCsv.read().decode("utf-8")
		lineas = archivo.split("\n")
		lista = []
		vectorNumeros = []
		mensaje = ""
		lineaArchivo = 1
		
		#Se recorren las lineas del archivo
		for linea in lineas:
			campos = linea.split(",")
			
			if lineaArchivo <= 1000:
				valorString = campos[0].strip()

				if valorString != "":
					try:
						valorEntero = int(campos[0])

						if valorEntero >= 0:
							vectorNumeros.append(valorEntero)
						else:
							mensaje += "<li>Línea " + str(lineaArchivo) + ": El valor no es un entero positivo</li>"
					except ValueError:
						mensaje += "<li>Línea " + str(lineaArchivo) + ": El valor no es un entero</li>"
				else:
					mensaje += "<li>Línea " + str(lineaArchivo) + ": Línea vacía</li>"

				lineaArchivo += 1

		if mensaje == "":
			try:
				vector = int(vector)

				for i, numero in enumerate(vectorNumeros, start = 1):
					#Datos para actualizar
					v = Vector.objects.get(id = i)
					
					if vector == 1:
						v.arreglo1 = numero
					elif vector == 2:
						v.arreglo2 = numero
					elif vector == 3:
						v.arreglo3 = numero
					elif vector == 4:
						v.arreglo4 = numero
					elif vector == 5:
						v.arreglo5 = numero
					else:
						v.arreglo6 = numero
					
					lista.append(v)
				
				#La transaccion hace que solo se produzca un update masivo
				with transaction.atomic():
					for v in lista:
						v.save()

				messages.success(request, "Vector " + str(vector) + " reemplazado exitosamente!")
			except Exception as ex:
				messages.error(request, "Se produjo un error al cargar el vector" + str(vector) + ": " + repr(ex))
		else:
			messages.info(request, "<ul>" + mensaje + "</ul>")
	except Exception as e:
		messages.error(request, "Se produjo un error al cargar el archivo: " + repr(e))

	return HttpResponseRedirect(reverse("cargarCSV"))

#Funcion para mostrar el histograma y seleccionar el tipo de distribucion
def histograma(request):
	validarConfiguracionInicial(obtenerCantidadVectores())
	totalVectores = obtenerTotalVectores()

	#Informacion de la pagina
	if request.method == "GET":
		try:
			#Se usa para generar la excepcion en el caso de que la tabla de vectores esta vacia
			vector = Vector.objects.get(id = 1)

			df = pd.DataFrame(list(Vector.objects.all().values()))
			arr = np.array(df)

			#Se busca si hay distribuciones almacenadas
			try:
				vectorDistribucion = VectorDistribucion.objects.get(id = 1)
				hayDistribucion = True
			except VectorDistribucion.DoesNotExist:
				vectorDistribucion = []
				hayDistribucion = False

			listaSuperior = []
			listaIzquierda = []
			ejesY = []
			mostrarGrafico = 1
			distribucionesLista = []
			distribucionesFormulario = [
				'Uniforme discreta', 
				'Uniforme continua', 
				'Normal', 
				'Binomial', 
				'Bernoulli', 
				'Arcoseno', 
				'Poisson', 
				'Logarítmica'
			]

			#Se generan los datos de los vectores
			for i in range(1, totalVectores + 1):
				arrEjeY = arr[:, i]
				ejeY = arrEjeY.tolist()
				ejesY.append(ejeY)

				#Se usa value_counts y no np.histogram debido a que numpy no estaba entregando valores exactos, sino aproximaciones por usar float
				listaVector = df['arreglo' + str(i)].value_counts()
				valores = listaVector.keys().tolist()
				cantidades = listaVector.tolist()
				listaX = []
				listaY = []

				for j in range(0, len(valores)):
					listaX.append(float(valores[j]))
					listaY.append(cantidades[j])

				listaIzquierda.append(listaX)
				listaSuperior.append(listaY)

				if hayDistribucion == True:
					distribucionesLista.append(getattr(vectorDistribucion, 'arreglo' + str(i)))
				else:
					distribucionesLista.append('')
		except Vector.DoesNotExist:
			listaSuperior = []
			listaIzquierda = []
			ejesY = []
			mostrarGrafico = 0

		return render(request, 'histograma.html', {
			'mostrarGrafico': mostrarGrafico, 
			'listaSuperior': listaSuperior, 
			'listaIzquierda': listaIzquierda, 
			'ejesY': ejesY,
			'distribucionesLista': distribucionesLista,
			'distribucionesFormulario': distribucionesFormulario
		})

	#Informacion del formulario
	try:
		#Guardar en la BD
		vector = int(request.POST["verGrafico"])
		distribucion = request.POST["distribucion"]
		otraDistribucion = request.POST.get("otraDistribucion", "")
		vector = vector + 1

		if otraDistribucion != "":
			distribucion = otraDistribucion

		try:
			#Se actualiza si existe
			v = VectorDistribucion.objects.get(id = 1)

			if vector == 1:
				v.arreglo1 = distribucion
			elif vector == 2:
				v.arreglo2 = distribucion
			elif vector == 3:
				v.arreglo3 = distribucion
			elif vector == 4:
				v.arreglo4 = distribucion
			elif vector == 5:
				v.arreglo5 = distribucion
			else:
				v.arreglo6 = distribucion
		except VectorDistribucion.DoesNotExist:
			#Se inserta si no existe
			if vector == 1:
				v = VectorDistribucion(id = 1, arreglo1 = distribucion)
			elif vector == 2:
				v = VectorDistribucion(id = 1, arreglo2 = distribucion)
			elif vector == 3:
				v = VectorDistribucion(id = 1, arreglo3 = distribucion)
			elif vector == 4:
				v = VectorDistribucion(id = 1, arreglo4 = distribucion)
			elif vector == 5:
				v = VectorDistribucion(id = 1, arreglo5 = distribucion)
			else:
				v = VectorDistribucion(id = 1, arreglo6 = distribucion)

		v.save()
		messages.success(request, "Distribución guardada exitosamente!")
	except Exception as e:
		messages.error(request, "Se produjo un error al guardar la distribución: " + repr(e))

	return HttpResponseRedirect(reverse("histograma"))
