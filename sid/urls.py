from django.urls import path

from . import views

#Urls y funciones a ejecutar
urlpatterns = [
    path('', views.index, name = 'index'),
    path('index', views.index, name = 'index'),
    path('generar', views.generar, name = 'generar'),
    path('visualizar', views.visualizar, name = 'visualizar'),
    path('gestionarDatos', views.gestionarDatos, name = 'gestionarDatos'),
    path('uploadCsv', views.uploadCsv, name = 'cargarCSV'),
    path('histograma', views.histograma, name = 'histograma')
]

handler404 = 'sid.views.handler404'
handler500 = 'sid.views.handler500'