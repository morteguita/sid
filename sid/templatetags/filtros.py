from django import template

register = template.Library()

@register.filter
def buscar_lista(lista, llave):
    return lista[llave]

@register.filter
def tamano_lista(lista):
	return len(lista)

@register.filter
def dividir_lista(lista, partes):
	return int(len(lista) / partes)