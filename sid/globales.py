from django.conf import settings

def global_settings(request):
    return {
        'CLASE_INTERFAZ': settings.CLASE_INTERFAZ,
        'CLASE_INTERFAZ_ALT': settings.CLASE_INTERFAZ_ALT
    }
