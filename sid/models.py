import datetime

from django.db import models

#Clase Vector
class Vector(models.Model):
	arreglo1 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo2 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo3 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo4 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo5 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo6 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)

	#Funcion para imprimir un registro como cadena usando print()
	def __str__(self):
		return 'x: {}, y1: {}, y2: {}, y3: {}, y4: {}, y5: {}, y6: {}'.format(self.id, self.arreglo1, self.arreglo2, self.arreglo3, self.arreglo4, self.arreglo5, self.arreglo6)


#Clase Configuracion
class Configuracion(models.Model):
	totalVectores = models.IntegerField(default = 1)
	vectorActual = models.IntegerField(default = 0)

	#Funcion para imprimir un registro como cadena usando print()
	def __str__(self):
		return 'Total vectores: {}, vector actual: {}'.format(self.totalVectores, self.vectorActual)


#Clase VectorModelo
class VectorModelo(models.Model):
	arreglo1 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo2 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo3 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo4 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo5 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)
	arreglo6 = models.DecimalField(default = -1, max_digits = 6, decimal_places = 2)

	#Funcion para imprimir un registro como cadena usando print()
	def __str__(self):
		return 'x: {}, y1: {}, y2: {}, y3: {}, y4: {}, y5: {}, y6: {}'.format(self.id, self.arreglo1, self.arreglo2, self.arreglo3, self.arreglo4, self.arreglo5, self.arreglo6)


#Clase VectorDistribucion
class VectorDistribucion(models.Model):
	arreglo1 = models.CharField(max_length = 255)
	arreglo2 = models.CharField(max_length = 255)
	arreglo3 = models.CharField(max_length = 255)
	arreglo4 = models.CharField(max_length = 255)
	arreglo5 = models.CharField(max_length = 255)
	arreglo6 = models.CharField(max_length = 255)

	#Funcion para imprimir un registro como cadena usando print()
	def __str__(self):
		return 'x: {}, y1: {}, y2: {}, y3: {}, y4: {}, y5: {}, y6: {}'.format(self.id, self.arreglo1, self.arreglo2, self.arreglo3, self.arreglo4, self.arreglo5, self.arreglo6)
